# Chapter 9
# Pointer

---

## Learning objectives
By the end of the chapoter, you should able to:

1. describe the concept of addresses and pointers
2. user indirect (`*`) and address operator (`&`) to show the value and address of a variable in a program

---

## Introduction

- equivalent as the *pronouns*
- *pointing* to the object, but not the object itself
  - e.g. Peter is a boy. He is 15 years old. He wears a shirt. It is write in colour.

![](Part2/Part2B/Chapter9/img/pronoun_and_pointer.png)

---

- imagine the pointer as a *road sign*
- pointing a a place, but not he place itselt

![](Part2/Part2B/Chapter9/img/pointer_as_road_sign.png)

---

## Variables and memory

- the value stored in memory has its own address
- variable identifiers are the symbolic representatnion of address

![](Part2/Part2B/Chapter9/img/variables_and_memory.png)

---

- use address operator (`&`) to show the address of the value
- use indirect operator (`*`) to show the data of corresponding address

![](Part2/Part2B/Chapter9/img/address_operator_and_indirection_operator.png)

---

- *pointer* is storing the memory address of the pointed variable

![](Part2/Part2B/Chapter9/img/pointer_and_address.png)

---

## Use of pointer in C

1. Pointer **declaration**
2. Pointer **initialization**
3. Accessing variables through pointers

---

### Pointer declaration

- syntax: `type* pointer_identifier;`
- pointer with different type are not compatible with each other

---

### Pointer initialization

- Pointer can be initialized
  1. At the pointer declaration, e.g.  
     `int x = 94;`  
     `int* p = &x;`
  2. In an assignment statement, e.g.   
     `int x = 94`;  
     `int* p;`  
     `*p = &x;`

---

### Accessing varaibles through pointers

- by using indirector (`*`) operator

![](Part2/Part2B/Chapter9/img/accessing_variables_through_pointers.png)

---

#### Example 1: do addition with pointers
![](Part2/Part2B/Chapter9/img/example1.png)

---

#### Example 2: read data with pointers
![](Part2/Part2B/Chapter9/img/example2.png)

---

#### Example 3: one pointer to access two variables
![](Part2/Part2B/Chapter9/img/example3.png)

---

#### Example 4: more than one pointer to access one variable
![](Part2/Part2B/Chapter9/img/example4.png)

---

#### Example 5: simulating paramters pass-by-reference
![](Part2/Part2B/Chapter9/img/example5.png)

---

## Pointers and arrays
- array is a sequence of data stored in consecutive memory location
- pointer and array can be used *interchangeably*

![](Part2/Part2B/Chapter9/img/pointers_and_arrays_example.png)

---

- integer occupies 4 bytes
  - the next entry has the address *greater than previous by 4*
- `arrayA` can be considered as the pointer to the first entry
  - the address of `arrayA` is 1234566
  - `*arrayA` is 6

---

### pointers defined and used as an array

![](Part2/Part2B/Chapter9/img/use_pointer_as_array.png)

---

- perform arimetic operation on pointer variables
  - `arrayA[0]` is the same as `*(arrayA+0)`
  - `arrayA[1]` is the same as `*(arrayA+1)`
  - `arrayA[2]` is the same as `*(arrayA+2)`
- the value of a pointer can be incremented (`++`) or decremented (`--`) to prints out the elements in an array

---

## Pointer functions

1. Define length of array in runtime
2. Free the memory

---

### Define length of array in runtime
- to define an integer array with the length defined by the user
  1. declare a pointer to intger `ptr`
  2. read an integer `x` entered by a user
  3. call a standard function `calloc` to alocate continguous memory size for `x` and assign the starting address to `ptr`

---

![](Part2/Part2B/Chapter9/img/calloc.png)

---

![](Part2/Part2B/Chapter9/img/define_length_of_arrays_in_runtime.png)

---

### Free the memory

- when memory location allocated by `calloc` are no longer needed, they should be freed by standard function `free` 
  - Usage: `free(pointer_identifier)`, e.g.  
    `int* arrayX;`   
    `araryX = (int*) calloc(x, sizeof(int));`  
    ...  
    `free(arrayX)`

---

## Pointer to pointer
- no limit level of indrection
  - e.g. `int***** pointer;`

![](Part2/Part2B/Chapter9/img/pointer_to_pointer.png)
