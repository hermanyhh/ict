# Chapter 7
# Functions

---

## Learning Objectives

By the end of this chapter, you should able to:
1. describe the concept of a function
2. declare, define and call a function in C
3. seperate a program with several functions

---

## Concept of function

- Like a *machine*
- Accepts inputs, and return *only one value* as output

![](Part2/Part2B/Chapter7/img/function_like_a_machine.png)

---

### Function in Mathematics

- $f(x)$ accepts and input $x$ and gives out output $x+2$

![](Part2/Part2B/Chapter7/img/mathematical_function_example.png)

|$x$| 4 | -2 | 0 | 2 | 10 |
|--:|:-:|:--:|:-:|:-:|:--:|
|$f(x)$|-2|0|2|4|12|

---

@snap[north]
## Function in C
@snapend

@snap[west span-50]
@ul
- C program is built up by functions
- At least one function, namely `main` function
@ulend
@snapend

@snap[east span-50]
![](Part2/Part2B/Chapter7/img/concept_of_a_function.png)
@snapend
---

### Purpose of a function
1. Accepts zero or more pieces of data
2. Operate on them, may have *side effect*
3. Return at most one piece of data by *return statement*

---

### Advantages of using functions

1. Understandable and manageable steps by having good function identifiers
2. Reusable codes by calling the functions
3. Can be collected as a library and used by others
4. Protect data within a function

---

### Designing a structured program

![](Part2/Part2B/Chapter7/img/structure_chart_for_a_C_program.png)

---

## Use of functions in C
- All functions must be **declared**, **defined** before **called**
- return *at most one value* by `return` statement
- *Execution sequence* as shown below

![](Part2/Part2B/Chapter7/img/declaring_calling_defining_functions.png)

---

### Function definition

![](Part2/Part2B/Chapter7/img/function_definition.png)

---

### Function definition - parameters

![](Part2/Part2B/Chapter7/img/formal_parameters_and_actual_parameters.png)

---

### Function definition - parameters

- **Formal parameters**: variables that are declared in the header of the function definition. 
- **Actual parameters** (often called arguments): the expressions in the calling statement.
- must match exactly in *type*, *order* and *number*, but names do not need to match

---

### Function definition - return type

each function should have the *return* type
  - same as the data type of return value
  - for the function with return value, they should declared as `void` type

---

### Function definition - return type

- functions with `void` type work as a statement only, e.g.
  - `greeting();`
  - `printOne(a);`
- function that return a value can be used in an expression, e.g.
  - `amt = getQuantity();`
  - `b = sqr(a)`

---

### Function definition - local variables

- *Local variables* are the variables that declared in the function. 
- they can be used within the function only.

![](Part2/Part2B/Chapter7/img/local_variable_scope.png)

---

### Function definition - Inter-function communication

- There are two implementation of passing the paramaters in common
  1. Pass by *value*
  2. Pass by *reference*

---

##### Pass by value

- value changed in the formal parameter will **NOT** affect the value of actual parameters.

![](Part2/Part2B/Chapter7/img/pass_by_value.png)

---

##### Pass by reference
- value changed in formal parameter will **ALSO** change the value of actual parameters
- there is no implementation of pass by reference in C
  - simulate by passing pointers

---

##### Pass by reference
![](Part2/Part2B/Chapter7/img/pass_by_value.png)

---

###### simulation of *more than one return values*
![](Part2/Part2B/Chapter7/img/simulating_more_than_one_return_values.png)

--- 

### function definition - return value
- the calling function receives the result fronm the called function
- only one value can be returned by the called function with `return` statement

---

## Function declaration
- should be *declared* in *global declaration* (before main function)
- Syntax
  `return_type function_name (formal_parameter_lsit);`

---

### Function call
- call the function by typing the defined and declaraed function name with corresponding number of parameters

![](Part2/Part2B/Chapter7/img/function_calls.png)

---

### Example 1 - void function without parameters

```C
#include <stdio.h>
// Function declaration
void sayHi (void);

int main (void)
{
    sayHi();
    return 0;
}

// Function definition
void sayHi (void)
{
    printf("Hi~");
}

```

---

### Example 1 - void function without parameters

Output:
```
Hi~
```

---

### Example 2 - void function with parameters

![](Part2/Part2B/Chapter7/img/example_2.1.png)
---

### Example 2 - void function with parameters

![](Part2/Part2B/Chapter7/img/example_2.2.png)

---

### Example 3 - non-void functions with parameter

![](Part2/Part2B/Chapter7/img/example_3.png)

---

## Standard functions
- a set of *standard functions* are written, collected as a **library**
- ready to called by other functions
- include the corresponding *header files* before using them

---

![](Part2/Part2B/Chapter7/img/library_functions_and_the_linker.png)

---

### `math.h`


- Absolute value (different versions for different types)
  -`int abs (int number);`  
    E.g. abs(3) returns 3
  - `long labs (long number);`
  - `float fabsf (float number);`  
    E.g. fabs(-3.4) returns 3.4
  - `double fabs (double number);`

---

### `math.h`

- Floor and ceiling functions
  - `double floor (double number);`  
     E.g. floor(1.9) returns 1.0  
     E.g. floor(1.1) returns -2.0
  - `float floorf (float number);`
  - `double ceil (double number);`  
    E.g. ceil(1.1) returns 2.0  
    E.g. ceil(-1.9) returns -1.0
  - `float ceilf (float number);`

---

### `math.h`

- Power
  - `double pow (double n1, double n2);`  
    E.g. pow(3.0, 4.0) returns 81.0  
    E.g. pow(3.4, 2.3) returns 16.687893
  - `float powf (float n1, float n2);`
- Square root
  - `double sqrt (double n1);`  
    E.g. sqrt(25) returns 5.0
  - `float sqrt (float n1);`

---
@snap[north-west span-100]
## Scope
@snapend

@snap[west span-50]
*Scope* determines the region of the program.
Variables are in scope from declaration until the end of their block.
@snapend

@snap[east span-50]
![](Part2/Part2B/Chapter7/img/scope_for_global_and_block_areas.png)
@snapend

---

## Exercise 1
The formula for converting Celsius temperatures to fahrenheit is
$$ F = 32 + C \frac{180.0}{100.0} $$

Write a program that ask the user to enter temperature reading in Celsius and then prints the equivalent Fahrenheit value. It then asks the user to enter a Fahrenheit value and prints out the equivalent Celsius. Provide separate functions as following diagram.

---

![](Part2/Part2B/Chapter7/img/exercise_1_chart.png)

---

## Exercise 2
Write a function to compute the perimeter and area of a right triangle when given the length of the two sides (a and b). The following formulas may be helpful.

$$ c^2 = a^2 + b^2 $$
$$ Area = 0.5 \times (a \times b) $$

---

## Exercise 3
Write a program that includes a function named *rounding*. The function takes two parameters, a float named number and an integer *place*. The function rounds the *number* in *place* decimal places and return that value. The main function should read a float and an integer, and then call the function and print out the result.

---

Example:

```
Enter a number: 12.3456
Rounding to how many decimal places? 3

The result is 12.346
```

